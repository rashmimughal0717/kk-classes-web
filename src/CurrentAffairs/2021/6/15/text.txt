






  ## ✅ हाल ही में संजय यादव को इलाहाबाद उच्च न्यायालय का मुख्य न्यायाधीश नियुक्त किया गया है 
![Justice Sanjay Yadav sworn in as Chief Justice of Allahabad HC - Hindustan  Times](https://images.hindustantimes.com/img/2021/06/13/550x309/b33d5b00-cc6d-11eb-95fb-e61383a731f5_1623605736353.jpg)

  ## ✅ हाल ही में ब्रिस्बेन शहर(ऑस्ट्रेलिया) को 2032 ओलिंपिक खेलो की मेजबानी के लिए चुना गया है  
![My Student City: Brisbane | Student World Online](https://www.studentworldonline.com/userfiles/images/Places/Australia/Australia_Brisbane_Google%20Maps.jpg)

  ## ✅ हाल ही में अमृत्य सेन ने 'होम इन द वर्ल्ड' नामक पुस्तक लिखी है 
![Home in the World&#39;: Amartya Sen&#39;s memoir to release in July - Times of India](https://timesofindia.indiatimes.com/thumb/msid-83377005,width-1200,height-900,resizemode-4/.jpg)

  ## ✅ हाल ही में अरुणा तवर पहले भारतीय ताईकांडो एथलिट बने है जिन्होंने टोक्यो ओलिंपिक के लिए क्वालीफाई किया है 
![21-year-old daughter of factory driver from Haryana gets Tokyo Paralympics  call up | Sports News,The Indian Express](https://images.indianexpress.com/2021/06/Untitled-design-5-1.jpg)







  ### **जून प्रतिदिन करंट अफेयर्स प्रैक्टिस सेट**
  👉 https://forms.gle/9BmF8pEKceeATvf67

  # **SSC GD Mock परीक्षा**
  👉 https://forms.gle/xXRvbGnenJYVkJweA

  ---


    
![100 number with targets and arrows ⬇ Stock Photo, Image by © Pixelery.com  #128731134](https://st3.depositphotos.com/1006899/12873/i/950/depositphotos_128731134-stock-photo-100-number-with-targets-and.jpg)  
   # **रूपए का पुरस्कार** 
   #### साप्ताहिक ऑनलाइन परीक्षा के प्रथम स्थान वाले छात्र को दिया जाता है 

 ---

![Change the game! | FOS Media Students' Blog](https://fos.cmb.ac.lk/blog/wp-content/uploads/2021/01/FacebookMotivation.jpg)
  # **पिछले सप्ताह के टॉपर्स**

  - 🥇राजेश पाल (बसुका)
  
  - 🥈अखिलेश यादव (दिलदारनगर)
  
  - 🥉सिकंदर प्रसाद (बिंदपुरवा)

  ---

# **हमारे पुरस्कृत छात्र** 
- **रेखा गुप्ता(अरंगी)** (1 बार)
- **राहुल सिंह (नवली)** (1 बार)
- **सिकंदर प्रसाद (बिंदपुरवा)** (1 बार) 
- **राजेश पाल (बसुका)** (1 बार) 

  ---
