









  ## ✅ हाल ही में नीरज ठाकुर अंडमान और निकोबार द्वीप के नए पुलिस महानिदेशक बने है
![News.in | Neeraj Thakur To Take Over As DGP, Andaman & Nicobar Islands: MHA  - Elets eGov - Elets](https://egov.eletsonline.com/wp-content/uploads/2022/03/Neeraj-Thakur.jpg)

  ## ✅ हाल ही में नई दिल्ली में भारत के सभी पूर्व प्रधानमंत्रियों का संग्रहालय बनाया गया है
![Prime Ministers' Museum Is Coming up in Nehru's Backyard, but He Won't Be  in It](https://cdn.thewire.in/wp-content/uploads/2018/10/18124339/supva-1024x683.jpg)

  ## ✅ हाल ही में केम्पेगौडा अंतराष्ट्रीय हवाईअडडे को 'विंग्स इंडिया पुरस्कार 2022' में सर्वेश्रेष्ठ अंतराष्ट्रीय हवाईअडडे का पुरस्कार दिया गया है
![Tender deadlines approach for key retail and dining contracts at Kempegowda  International Airport T2 : The Moodie Davitt Report -The Moodie Davitt  Report](https://www.moodiedavittreport.com/wp-content/uploads/2021/10/New-Bangalore-T2.png)

  ## ✅ हाल ही में केंद्रीय वाणिज्य और उद्योग मंत्री पियूष गोयल ने दुबई(UAE) में भारतीय आभूषण प्रदर्शनी केंद्र भवन का उद्घाटन किया है
![Patients should use as much oxygen as they need, says Piyush Goyal amid  criticism - The Week](https://www.theweek.in/content/dam/week/news/india/images/2021/3/29/piyush-goyal.jpeg)

  ## ✅ हाल ही में के श्याम प्रसाद ने 'पूर्ति प्रदत श्री सोमैया' नामक पुस्तक लिखा है
![Shyama Prasad Choudhury | Deccan Chronicle](https://s3.ap-southeast-1.amazonaws.com/images.deccanchronicle.com/dc-Cover-t7d05blc2uv91ip3bs19a6rf35-20180830023734.jpeg)

  ## ✅ हाल ही में सुमित्रा गाँधी कुलकर्णी ने 'मोदी स्टोरी' नामक वेबसाइट लांच किया है
![Inspiring Journey Of The PM: बीजेपी नेता और मंत्रियों ने शेयर की Modi Story;  जानें क्या है इसकी खासियत - Republic Bharat](https://img.republicworld.com/republic-prod/stories/images/xxhdpi/1648284837623ed4a5da0ee.jpeg?tr=w-758,h-433)

  ## ✅ हाल ही में Wilfried Brutsaert को स्टॉकहोल्म जल पुरस्कार विजेता 2022 के रूप में नामित किया गया है
![Evaporation expert, Wilfried Brutsaert, wins Stockholm Water Prize 2022 |  SIWI - Leading expert in water governance](https://siwi.org/wp-content/uploads/2022/03/swplaureate2022-professoremerituswilfried-h-brutsaert.jpg)

  ## ✅ हाल ही में रेनू सिंह वन अनुसंधान संसथान की निदेशक बनी है
![IFS officer Renu Singh appointed FRI director | Dehradun News - Times of  India](https://static.toiimg.com/thumb/msid-90504399,imgsize-45038,width-400,resizemode-4/90504399.jpg)









  # **[प्रतिदिन प्रैक्टिस सेट]**
  👉 https://forms.gle/dZP1MpruLEm2UgB3A
