import React, { useState } from 'react';

import {
  Carousel,
} from 'react-bootstrap';

import './styles.scss';

const HeroCarousel = ({
  Images,
}) => {
  const [index, setIndex] = useState(0);

  const handleSelect = (selectedIndex, e) => {
    setIndex(selectedIndex);
  };

  return (
    <Carousel
      className="heroCarousel"
      activeIndex={index} onSelect={handleSelect}
    >
      { Images.map( (image, index) => (
        <Carousel.Item key={index}>
          <img
            src={image.src}
            alt={image.title}
          />
          <Carousel.Caption>
            <h3>{image.title}</h3>
          </Carousel.Caption>
        </Carousel.Item>
      ))}
    </Carousel>
  );
}

export default HeroCarousel;
