


  ## ✅ हाल ही में UAE देश में एशियाई मुक्केबाज़ी चैंपियनशिप 2021 का आयोजन किया जायेगा
![United Arab Emirates | Geography and Maps | Goway](https://www.goway.com/media/cache/bb/b7/bbb70d150aa7c9bfa3c1687c319542e0.jpg)

  ## ✅ हाल ही में भारत के पूर्व अटार्नी जनरल 'सोली सोराबजी' का निधन हो गया है 
![Former Attorney General Soli Sorabjee, 91, Passes Away Due To COVID-19](https://images.moneycontrol.com/static-mcnews/2021/04/Soli-Sorabjee-770x433.jpg)

  ## ✅ हाल ही में शूटर दादी के नाम से मशहूर चंद्रो तोमर का निधन हो गया है 
![Chandro Tomar aka Shooter Dadi dies of COVID-19; netizens mourn loss -  IBTimes India](https://data1.ibtimes.co.in/en/full/760602/shooter-dadi-no-more.jpg)

  ## ✅ हाल ही में कमला हैरिश 'मैडम तुसाद वैक्स म्यूजियम' में शामिल होने वाली पहली अमेरिकी महिला उपराष्ट्रपति बानी है 
![Kamala Harris becomes first US vice president to get a wax figure in Madame  Tussauds, World News | wionews.com](https://cdn.wionews.com/sites/default/files/styles/story_page/public/2021/05/02/193507-kamala.JPG)




  ---

  ### 👨‍💻👩‍💻 **मई 2021 प्रतिदिन ऑनलाइन करंट अफेयर्स प्रैक्टिस सेट**
  👉 https://forms.gle/XgHzrQHqjYoC9nAe8

  ---

  ### 👨‍💻👩‍💻 **अप्रैल करंट अफेयर्स प्रैक्टिस सेट**
  👉 [https://forms.gle/cz3FcLmnL7KMNj8h7](https://forms.gle/cz3FcLmnL7KMNj8h7)

  ---

  ### 📝 **इस सप्ताह का ऑनलाइन परीक्षा**

  👉 [https://forms.gle/UmgmPEGhsvhPrDHdA](https://forms.gle/UmgmPEGhsvhPrDHdA)

  ---
![Hindi Quotes on Education | शिक्षा पर हिंदी कोट्स](https://www.duniyahaigol.com/wp-content/uploads/2017/09/education-quotes-in-hindi.jpg)
  ## **पिछले सप्ताह के ऑनलाइन परीक्षा के परिणाम**
  
  **1️⃣🥇** - रेखा गुप्ता (अरंगी)
  
  **2️⃣🥈** - बबलू सिंह यादव (सूर्यभानपुर)
  
  **3️⃣🥉** - ज्योति शर्मा (मुहम्मदपुर)

  ---
