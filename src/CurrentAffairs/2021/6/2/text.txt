
    
![100 number with targets and arrows ⬇ Stock Photo, Image by © Pixelery.com  #128731134](https://st3.depositphotos.com/1006899/12873/i/950/depositphotos_128731134-stock-photo-100-number-with-targets-and.jpg)  
   # **रूपए का पुरस्कार** 
   #### साप्ताहिक ऑनलाइन परीक्षा के प्रथम स्थान वाले छात्र को दिया जाता है 


  ---
  
# **हमारे पुरस्कृत छात्र** 
- **रेखा गुप्ता(अरंगी)** (1 बार)
- **राहुल सिंह (नवली)** (1 बार)



  ---




  ## ✅ हाल ही में पूजा रानी(भारतीय महिला बॉक्सर) ने एशिआई बॉक्सिंग चैंपियनशिप 2021 में स्वर्ण पदक जीता है  
![Pooja Rani strikes gold; silver for Mary Kom, 2 others at Asian Boxing  Championships | Sports News,The Indian Express](https://images.indianexpress.com/2021/05/Pooja-Win-Boxing_571_855.jpg)

  ## ✅ हाल ही में चीन देश ने 'तीन बच्चो' की निति को लागु कर दिया है 
![China relaxes strict two-child policy: Urban - One News Page VIDEO](https://video.newsserve.net/v/20210531/1320294997-China-relaxes-strict-two-child-policy-Urban-Chinese_hires.jpg)

  ## ✅ हाल ही में पंजाब राज्य ने 'उड़ान' योजना को लांच किया है 
![Punjab Map, Districts in Punjab | Map, Punjab, Sangrur](https://i.pinimg.com/originals/6c/18/38/6c183816a33469a8530889428091fa60.jpg)




  ---

![SSC GD Constable Test Series, Paper & Questions](https://www.careerpower.in/images/ssc-gd-exam-mobile.png)
  # **SSC GD Mock परीक्षा**

👉 [https://forms.gle/Sir2Qr8KEPx4csZC8](https://forms.gle/Sir2Qr8KEPx4csZC8)
  
 ---

![Top Current Affairs 2019 with answers: September 3 to September 9, 2019 -  Education Today News](https://akm-img-a-in.tosshub.com/indiatoday/images/story/201909/current_affairs_new_0.png?8dmp2f4HrwVzAaVuMKuF9RJ_HkBmoZGT&size=1200:675)
  
  ### **जून प्रतिदिन करंट अफेयर्स प्रैक्टिस सेट**
  👉 https://forms.gle/9BmF8pEKceeATvf67

  ### **मई करंट अफेयर्स प्रैक्टिस सेट**
  👉 https://forms.gle/XgHzrQHqjYoC9nAe8

  ### **अप्रैल करंट अफेयर्स प्रैक्टिस सेट**
  👉 [https://forms.gle/cz3FcLmnL7KMNj8h7](https://forms.gle/cz3FcLmnL7KMNj8h7)

  ---

![Change the game! | FOS Media Students' Blog](https://fos.cmb.ac.lk/blog/wp-content/uploads/2021/01/FacebookMotivation.jpg)
  # **पिछले सप्ताह के टॉपर्स**
  
  **🥇** - राहुल सिंह (नवली)
  
  **🥈** - रेखा गुप्ता (अरंगी)
  
  **🥉** - अखिलेश यादव (दिलदारनगर)

  ---

