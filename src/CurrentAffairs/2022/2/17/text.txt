






  ## ✅ हाल ही में भारतीय अंतरिक्ष अनुसंधान संगठन ने वर्ष 2022 के पहले पृथ्वी अवलोकन उपग्रह EOS-04 को PSLV-C52 रॉकेट के द्वारा लांच किया है
![Countdown begins for launch of ISRO&#39;s PSLV-C52/EOS-04 mission from SHAR](https://assets.thehansindia.com/h-upload/2022/02/13/1276524-pslv.webp)

  ## ✅ हाल ही में महाराष्ट्र राज्य सरकार के स्वास्थ मंत्री ने कैंसर की रोकथाम के लिए भारत की पहली 'होप एक्सप्रेस' मशीन को लांच किया है
![राज्य समसामयिकी 1 (14-February-2022)महाराष्ट्र के स्वास्थ्य मंत्री ने कैंसर  से बचाव के लिए](https://www.civilhindipedia.com/upload/blog/post/blog21_maharashtra-health-minister-announces-hope-express-for-cancer-prevention.jpeg)

  ## ✅ हाल ही में मनोज तिवारी बिहार राज्य के खादी और हस्तशिल्प के नए ब्रांड एम्बेसडर नियुक्त हुए है
![BJP&#39;s Manoj Tiwari to hold march against ban on Chhat Puja celebrations |  Latest News Delhi - Hindustan Times](https://images.hindustantimes.com/img/2021/10/08/550x309/e963f696-22f1-11ec-8eb3-f9ad79e07ca9_1633677945351_1633677953267.jpg)

  ## ✅ हाल ही में वर्ष 2022 में मुंबई अंतराष्ट्रीय फिल्म महोत्सव के 17वे संस्करण का आयोजन किया गया है
![17th edition of Mumbai International Film Festival to be held form May](https://st.adda247.com/https://wpassets.adda247.com/wp-content/uploads/multisite/sites/5/2022/02/14081154/film-750x430-1.jpg)

  ## ✅ हाल ही में नितिन गडकरी ने बिहार में गंगा नदी पर लंबे रेल सह सड़क पुल का उदघाटन किया है
![Nitin Gadkari: केंद्रीय मंत्री नितिन गडकरी ने बिहार में गंगा नदी पर &#39;मुंगेर  रेल-सह-सड़क पुल&#39; - &#39;श्री कृष्ण सेतु&#39; को आम जन को समर्पित किया : Union  Minister Nitin Gadkari dedicates &#39;Munger Rail-cum-Road Bridge&#39; - &#39;Shri  Krishna Setu&#39; to common people on the river ...](https://static.langimg.com/thumb/msid-88479647,imgsize-980785,width-1600,height-900,resizemode-75/navbharat-times.jpg)







  # **[प्रतिदिन प्रैक्टिस सेट]**
  👉 https://forms.gle/RC2yHCmgxtDxLvqR7
