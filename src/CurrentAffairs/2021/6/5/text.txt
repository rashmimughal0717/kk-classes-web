
    
![100 number with targets and arrows ⬇ Stock Photo, Image by © Pixelery.com  #128731134](https://st3.depositphotos.com/1006899/12873/i/950/depositphotos_128731134-stock-photo-100-number-with-targets-and.jpg)  
   # **रूपए का पुरस्कार** 
   #### साप्ताहिक ऑनलाइन परीक्षा के प्रथम स्थान वाले छात्र को दिया जाता है 


  ---
  
# **हमारे पुरस्कृत छात्र** 
- **रेखा गुप्ता(अरंगी)** (1 बार)
- **राहुल सिंह (नवली)** (1 बार)



  ---







  ## ✅ हाल ही में 'इसहाक हर्जोग' इजराइल देश के नए राष्ट्रपति बने है 
![Amid tension with Palestine, Israel appoints Isaac Herzog as new President  | Hindustan Times](https://images.hindustantimes.com/img/2021/06/03/1600x900/AFP_9B89GP_1622699936440_1622699964777.jpg)

  ## ✅ हाल ही में महाराष्ट्र राज्य ने 'कोरोना मुक्त गाँव' प्रतियोगिता की घोषणा की है
![Maharashtra Map, Districts in Maharashtra | India world map, Map, Geography  map](https://i.pinimg.com/originals/ba/2d/a2/ba2da274afe362c68c55a26d1185ebeb.jpg)







  ---

![SSC GD Constable Test Series, Paper & Questions](https://www.careerpower.in/images/ssc-gd-exam-mobile.png)
  # **SSC GD Mock परीक्षा**

👉 [https://forms.gle/Sir2Qr8KEPx4csZC8](https://forms.gle/Sir2Qr8KEPx4csZC8)
  
 ---

![Top Current Affairs 2019 with answers: September 3 to September 9, 2019 -  Education Today News](https://akm-img-a-in.tosshub.com/indiatoday/images/story/201909/current_affairs_new_0.png?8dmp2f4HrwVzAaVuMKuF9RJ_HkBmoZGT&size=1200:675)
  
  ### **जून प्रतिदिन करंट अफेयर्स प्रैक्टिस सेट**
  👉 https://forms.gle/9BmF8pEKceeATvf67

  ### **मई करंट अफेयर्स प्रैक्टिस सेट**
  👉 https://forms.gle/XgHzrQHqjYoC9nAe8

  ### **अप्रैल करंट अफेयर्स प्रैक्टिस सेट**
  👉 [https://forms.gle/cz3FcLmnL7KMNj8h7](https://forms.gle/cz3FcLmnL7KMNj8h7)

  ---

![Change the game! | FOS Media Students' Blog](https://fos.cmb.ac.lk/blog/wp-content/uploads/2021/01/FacebookMotivation.jpg)
  # **पिछले सप्ताह के टॉपर्स**
  
  **🥇** - राहुल सिंह (नवली)
  
  **🥈** - रेखा गुप्ता (अरंगी)
  
  **🥉** - अखिलेश यादव (दिलदारनगर)

  ---

