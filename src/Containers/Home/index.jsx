import React from 'react';

import {
  Container,
  Card,
} from 'react-bootstrap';

import ImgArmy from 'static/Army.jpg';
import ImgIAF1 from 'static/IAF1.jpg';
import ImgNavy from 'static/IndianNavy.jpg';
import ImgNavy1 from 'static/IndianNavy1.jpg';
import ImgBSF from 'static/BSF.webp';
import ImgITBP from 'static/ITBP.jpg';
import ImgCISF from 'static/CISF.jpg';
import ImgASR from 'static/ASR.jpg';
import ImgASR1 from 'static/ASR1.jpg';
import ImgNSG from 'static/NSG.jpg';

import HeroCarousel from 'Components/HeroCarousel';

import './styles.scss';

const Images = [
  {
    src: ImgArmy,
    title: "Indian Army",
  },
  {
    src: ImgIAF1,
    title: "Indian Air Force",
  },
  {
    src: ImgNavy1,
    title: "Indian Navy",
  },
  {
    src: ImgNavy,
    title: "Indian Navy",
  },
  {
    src: ImgBSF,
    title: "Border Security Force",
  },
  {
    src: ImgITBP,
    title: "Indo-Tibetan Border Police",
  },
  {
    src: ImgCISF,
    title: "Central Industrial Security Force",
  },
  {
    src: ImgASR,
    title: "Assam Rifles",
  },
  {
    src: ImgASR1,
    title: "Assam Rifles",
  },
  {
    src: ImgNSG,
    title: "National Security Guard",
  },
];

const Home = () => {
  return (
    <div className="home-page">
      <HeroCarousel
        Images={Images}
      />
      <div style={{backgroundColor: "#aee2ff"}} className="py-5">
        <h5 className="text-center">
          <b>
            लक्ष्य कोई बड़ा नहीं, हारा वही जो लड़ा नहीं
          </b>
        </h5>
      </div>
      <Container className="mt-5 quoteCard">
      <div>
        <h2 className="text-center">Time Table </h2>
        <ul>
          <li>
            दैनिक और मासिक करंट अफेयर्स प्रैक्टिस सेट के साथ
          </li>
          <li>
            रोजाना शाम 7:00 बजे सामान्य अध्ययन
          </li>
          <li>
            प्रत्येक सोमवार साप्ताहिक ऑनलाइन परीक्षा
          </li>
          <li>
            प्रत्येक महीने की शुरुआत में मासिक ऑनलाइन परीक्षा
          </li>
          <li>            
            आप अपने प्रश्नो को बिना झिझक यहाँ पूछ सकतें है 
          </li>
        </ul>
      </div>
      </Container>
    </div>
  );
}

export default Home;
