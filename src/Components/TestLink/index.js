import React from 'react';

import {
  ProgressBar
} from 'react-bootstrap';

import IconNew from 'static/icons/new.png';

import './styles.scss';

const TestLink = ({
  title,
  links,
}) => (
  <div className="test-link mb-4">
    <h3 className="text-center">
      <b>
        {title}
      </b>
    </h3>
    <div>
      {
        links.map(
          (test, index) => 
          <>
            <a href={test.link}>
              <span>👉</span> <small><i>{test.link}</i></small> { index === 0 && <img src={IconNew} alt="new"/>}
            </a>
            <br/>
            {
              test.target &&
              <div className="target-banner">
                <span>कुल छात्रों ने परीक्षा दिया: {test.achieved}/{test.target}</span>
                <ProgressBar animated={!index} now={test.achieved / test.target * 100} />
                <br/>
                <br/>
              </div>
            }
          </>
        )
      }
    </div>
  </div>
)

export default TestLink;
