













  ## ✅ हाल ही में अल्केश कुमार शर्मा इलेक्ट्रॉनिक्स और सुचना प्रौद्योगीकी मंत्रालय के नए सचिव बने है
![MeitY Secretary: Alkesh Kumar Sharma appointed as new MeitY secretary - The  Economic Times](https://img.etimg.com/thumb/msid-91288988,width-650,imgsize-346443,,resizemode-4,quality-100/alkesh-kumar-sharma.jpg)

  ## ✅ हाल ही में विएतनाम देश में दुनिया का सबसे लंबा कांच का पुल 'सफ़ेद ड्रैगन' खोला गया है
![vietnam Bach Long bridge White Dragon world's longest glass-bottomed bridge  | White Dragon Bridge: दुनिया का सबसे लंबा कांच का पुल, जिस पर चल तो  पाएंगे; बस नीचे देखना मना है... |](https://hindi.cdn.zeenews.com/hindi/sites/default/files/2022/05/04/1129917-dp1.jpg)

  ## ✅ हाल ही में महाराष्ट्र राज्य सरकार ने जेल में बंद कैदियों के लिए 'जीवला' नामक ऋण योजना को शुरू किया गया है
![Maharashtra: Scheme to provide loans to inmates in state prisons launched |  Cities News,The Indian Express](https://images.indianexpress.com/2022/05/prison.jpg)






  # **[प्रतिदिन प्रैक्टिस सेट]**
  👉 https://forms.gle/cR3bZdaugCrsnNWY8
