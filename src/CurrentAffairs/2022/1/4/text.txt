











  ## ✅ हाल ही में निकिता सकल ने मिसेज इंडिया गैलेक्सी 2021 का खिताब जीता है
![राळेगण सिद्धीची निकिता पोटे झाली &quot;मिस इंडिया&quot; | Sakal](https://gumlet.assettype.com/esakal/import/s3fs-public/news-story/cover-images/0_E0_A4_A8_E0_A4_BF_E0_A4_95_E0_A4_BF_E0_A4_A4_E0_A4_BE_20_E0_A4_AA_E0_A5_8B_E0_A4_9F_E0_A5_87.jpg?compress=true&fit=max&format=auto&dpr=1.0&w=400)

  ## ✅ हाल ही में सिक्किम राज्य में PM नरेंद्र मोदी के नाम पर 'नरेंद्र मोदी मार्ग' सड़क का उदघाटन किया गया है
![നെഹ്റു റോഡ് ഇനിമുതല്‍ നരേന്ദ്രമോദി റോഡ്; പേര് മാറ്റി സിക്കിം സര്‍ക്കാര്‍ |  Sikkim Governor Ganga Prasad inaugurates road named after PM Modi](https://static-ai.asianetnews.com/images/01fr4p1c9ce716npbnjdpk08ff/neharu-road-become-modi-road_710x400xt.jpg)

  ## ✅ हाल ही में संघाई चीन में दुनिया की सबसे लम्बी रेल लाइन की शुरुआत की गयी है
![China launches longest bullet train - The Hindu](https://www.thehindu.com/news/international/article16993399.ece/ALTERNATES/LANDSCAPE_1200/05IN_THVLR_LONGEST_BULLET_TRAIN)

  ## ✅ हाल ही में नागपुर महाराष्ट्र में सार्वजनिक शौचालय की स्थिति सुधारने के लिए 'Right To Pee' अभियान की शुरुआत की गयी है
![Right to pee activists protest, detained](https://cdn.dnaindia.com/sites/default/files/styles/full/public/2017/11/20/626099-right-to-pee.jpg)

  ## ✅ हाल ही में वी के त्रिपाठी को रेलवे बोर्ड का नया CEO नियुक्त किया गया है
![VK Tripathi appointed chairman and CEO of Railway Board - India News](https://akm-img-a-in.tosshub.com/indiatoday/images/story/202201/vk_tripathi_1200x768.png?PW0UulBW8POy9fVDqJTF0ina3qJNdcxE&size=770:433)



  # **[प्रतिदिन प्रैक्टिस सेट]**
  👉 https://forms.gle/fC8ae8ivea4gp7zq8
