import React from 'react';
import {
  Alert,
  Container,
} from 'react-bootstrap';

import TestLink from 'Components/TestLink';
import Winners from 'Components/Winners';

import './styles.scss';

const MOCK_TEST_NRA_CET = [
  {
    link: 'Not Avliable Now',
    target: 200,
    achieved: 0,                  
  },
];

const UP_SSSC_PET = [
  {
    link: 'Not Avliable Now',
    target: 200,
    achieved: 0,
  },
];

const WINNERS = [  
  'ओपिन पासवान (सूर्यभानपुर)',
  'संतोश प्रजापति (सूर्यभानपुर)',
  'रंजन कुमार (केशोपुर)',
];

const ALL_WINNERS = [
  'ओपिन पासवान (सूर्यभानपुर) (3 बार)',   
];

const ALL_LOTTERY_WINNERS = [
  'NAME (VILLAGE) (1 बार)',
];

const AboutUs= () => {
  return (
    <div className="mock-test-wrapper">
      <div className="text-center bg-light-blue p-2">
        <h2>
          <b>
            पिछले सप्ताह के टॉपर्स
          </b>
        </h2>
        <marquee>
          <h6 className="mt-3">
<b>KK CLASSES</b> दिलदारनगर (स्टेट बैंक के पास)  📱7827173383, 9161130281
          </h6>
        </marquee>
      </div>

      <div className="pt-3">
        <Container>
          <Winners
            winners={WINNERS}
          />
        </Container>
      </div>
      <hr/>

      <Container className="pt-1">
        <TestLink
        title="NRA CET"
        links={MOCK_TEST_NRA_CET}
        />
        <hr/>

        <TestLink
        title="UP SSSC PET"
        links={UP_SSSC_PET}
        />
        <hr/>

      </Container>
      <div>
        <h2 className="text-center py-2" style={{backgroundColor: "#aee2ff"}}>
          <b>
            TOPPERS
          </b>
        </h2>
      </div>
      <Container>
        <ul className="text-muted">
          {
            ALL_WINNERS.map(
              winner => (
                <li>
                  { winner }
                </li>
              )
            )
          }
        </ul>
      </Container>
      <div>
        <h2 className="text-center py-2" style={{backgroundColor: "#aee2ff"}}>
          <b>
            LOTTERY WINNERS
          </b>
        </h2>
      </div>
      <Container>
        <ul className="text-muted">
          {
            ALL_LOTTERY_WINNERS.map(
              winner => (
                <li>
                  { winner }
                </li>
              )
            )
          }
        </ul>
      </Container>
    </div>
  )
}

export default AboutUs;
