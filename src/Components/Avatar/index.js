import React from 'react';

import { Image } from 'react-bootstrap';

import './styles.scss';

const Avatar = ({
  src,
  name,
  subject,
}) => (
  <div className="avatar">
    <Image src={src} roundedCircle />
    <div className="name">{name}</div>
    <div className="subject">{subject}</div>
  </div>
)

export default Avatar;
