import React from 'react';

import WhatsApp from 'static/whatsapp.svg';
import FaceBook from 'static/facebook.svg';
import Phone from 'static/phone.svg';

import './styles.scss';

const Footer = () => (
  <div  className="footer">
    <div className="contactInfo">
      <div className="title">
        संपर्क करें
      </div>
      <a href="tel:7827173383">
        <img src={Phone} alt="Phone icon" />
      </a>
      <a href="https://wa.me/qr/RB6V3KW4GJCNB1">
        <img src={WhatsApp} alt="whatsapp icon" />
      </a>
      <a href="https://www.facebook.com/KK-Classes-110083153964654">
        <img src={FaceBook} alt="Facebook icon" />
      </a>
    </div>
    <small>
      KK CLASSES दिलदारनगर (स्टेट बैंक के पास)
    </small>
    <small>
      7827173383, 9161130281
    </small>
    <small className="copyright">
      ©2021 KK CLASSES
    </small>
  </div>
)

export default Footer;
