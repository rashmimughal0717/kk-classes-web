




















  ## ✅ हाल ही में जारी ग्लोबल हंगर इंडेक्स में 116 देशो में भारत का स्थान 101वा रहा है, इस इंडेक्स को Welthungerhilfe द्वारा जारी किया जाता है
![Global Hunger Index 2021 - India slips to 101st rank behind Pakistan,  Bangladesh, Nepal | UPSC SDGs - YouTube](https://i.ytimg.com/vi/gSgUiQ-xo0c/maxresdefault.jpg)

  ## ✅ हाल ही में विराट कोहली को 'फायर बोल्ट' कंपनी का ब्रांड एम्बेसडर नियुक्त किया गया है
![Virat Kohli joins Fire-Boltt as brand ambassador | SportsMint Media](https://sportsmintmedia.com/wp-content/uploads/2021/10/India-wearable-brand-Fire-Boltt-signs-Virat-Kohli-as-brand-ambassador.jpg)

  ## ✅ हाल ही में हमने 16 अक्टूबर को विश्व खाद्य दिवस मनाया, इसका थीम Safe Food Now For A Healthy Tomorrow(स्वस्थ कल के लिए अब सुरक्षित भोजन) था
![World Food Day 2021 - October 16](https://affairscloud.com/assets/uploads/2021/10/World-Food-Day.jpg)

  ## ✅ हाल ही में राजस्थान के जयपुर शहर में भारत का पहला अटल सामुदायिक नवाचार केंद्र शुरू किया गया है
![नीति आयोग अटल नवाचार मिशन (एआईएम) के अटल समुदाय नवाचार केन्द्र (एसीआईसी)  कार्यक्रम की शुरूआत एसीआईसी समाज की जरूरतों को पूरा करने के लिए नवाचार को  प्रोत्साहन प्रदान करेगाः श्री धर्मेन्द्र प्रधान Posted On: 31 JUL 2019  4:01PM by PIB Delhi भारत में ...](https://static.pib.gov.in/WriteReadData/userfiles/image/image002GYDD.jpg)

  ## ✅ हाल ही में रिलायंस कंपनी ने फ़ोबस की विश्व की नियोक्ता रैंकिंग में प्रथम स्थान प्राप्त किया है
![Reliance Tops India Inc World&#39;s Best Employer Rankings 2021](https://www.pgurus.com/wp-content/uploads/2021/10/Reliance-tops.jpg)

  ## ✅ हाल ही में दिल्ली की पुलिस ने 'तेजस्विनी' पहल की शुरुआत की है
![Delhi: अपराधियों के लिए काल बनी &#39;Tejaswini&#39; टीम, महिला सुरक्षा के लिए कर  रही काम | वनइंडिया हिंदी - video Dailymotion](https://s2.dmcdn.net/v/TJ-JR1XP6A5RmzFcf/x1080)





  # **[प्रतिदिन प्रैक्टिस सेट]**
  👉 https://forms.gle/QRgPHQGH9dW4EBKS7
