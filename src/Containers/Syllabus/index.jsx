import {
  Container
} from 'react-bootstrap';

import MarkDownText from 'Components/MarkDownText';

import './styles.scss';

const CurrentAffairs = () => {
  return (
  <>
    <Container>
      <MarkDownText
        title="SYLLABUS(पाठ्यक्रम)"
        filePath={`syllabus.txt`}
      />
    </Container>
  </>
  );
}

export default CurrentAffairs;
