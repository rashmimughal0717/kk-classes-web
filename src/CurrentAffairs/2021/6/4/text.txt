
    
![100 number with targets and arrows ⬇ Stock Photo, Image by © Pixelery.com  #128731134](https://st3.depositphotos.com/1006899/12873/i/950/depositphotos_128731134-stock-photo-100-number-with-targets-and.jpg)  
   # **रूपए का पुरस्कार** 
   #### साप्ताहिक ऑनलाइन परीक्षा के प्रथम स्थान वाले छात्र को दिया जाता है 


  ---
  
# **हमारे पुरस्कृत छात्र** 
- **रेखा गुप्ता(अरंगी)** (1 बार)
- **राहुल सिंह (नवली)** (1 बार)



  ---






  ## ✅ हाल ही में अरुण कुमार मिश्रा को राष्ट्रीय मानव अधिकार आयोग(NHRC) का नया अध्यक्ष नियुक्त किया गया है  
![Live Law on Twitter: &quot;Former Supreme Court judge, Justice Arun Kumar Mishra  today took over as the Chairman of the National Human Rights Commission  (NHRC). Justice Mishra had retired from the Supreme](https://pbs.twimg.com/media/E23y_adWEAAETYN.jpg)

  ## ✅ हाल ही में प्रदीप चंद्रन नायर को असम राइफल का महानिदेशक नियुक्त किया गया है 
![Lieutenant General Pradeep Chandran Nair takes over as Director General  Assam Rifles - The Economic Times](https://m.economictimes.com/thumb/msid-83147056,width-1200,height-900,resizemode-4,imgsize-634562/pradeep-chandran-nair-.jpg)

  ## ✅ हाल ही में WHO ने भारत में पाए जाने वाले Covid 19 वैरिएंट(B.1.6.1.7.1) को काप्पा(Kappa) और डेल्टा(डेल्टा) नाम दिया है   
![Covid: WHO renames UK and other variants with Greek letters - BBC News](https://ichef.bbci.co.uk/news/640/cpsprodpb/9656/production/_118768483_coronavirus_variant_names-2x-nc.png)

  ## ✅ हाल ही में चीन देश में दुनिया का पहला मानव बर्ड फ्लू(H10N3) का मामला सामने आया है  
![International Relations with China in 2021 | China map, India map, Asia map](https://i.pinimg.com/originals/a3/8e/cb/a38ecb505863e0af9016c0007f6ec74f.jpg)







  ---

![SSC GD Constable Test Series, Paper & Questions](https://www.careerpower.in/images/ssc-gd-exam-mobile.png)
  # **SSC GD Mock परीक्षा**

👉 [https://forms.gle/Sir2Qr8KEPx4csZC8](https://forms.gle/Sir2Qr8KEPx4csZC8)
  
 ---

![Top Current Affairs 2019 with answers: September 3 to September 9, 2019 -  Education Today News](https://akm-img-a-in.tosshub.com/indiatoday/images/story/201909/current_affairs_new_0.png?8dmp2f4HrwVzAaVuMKuF9RJ_HkBmoZGT&size=1200:675)
  
  ### **जून प्रतिदिन करंट अफेयर्स प्रैक्टिस सेट**
  👉 https://forms.gle/9BmF8pEKceeATvf67

  ### **मई करंट अफेयर्स प्रैक्टिस सेट**
  👉 https://forms.gle/XgHzrQHqjYoC9nAe8

  ### **अप्रैल करंट अफेयर्स प्रैक्टिस सेट**
  👉 [https://forms.gle/cz3FcLmnL7KMNj8h7](https://forms.gle/cz3FcLmnL7KMNj8h7)

  ---

![Change the game! | FOS Media Students' Blog](https://fos.cmb.ac.lk/blog/wp-content/uploads/2021/01/FacebookMotivation.jpg)
  # **पिछले सप्ताह के टॉपर्स**
  
  **🥇** - राहुल सिंह (नवली)
  
  **🥈** - रेखा गुप्ता (अरंगी)
  
  **🥉** - अखिलेश यादव (दिलदारनगर)

  ---

