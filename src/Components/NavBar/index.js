
import React from 'react';

import { Navbar, Nav, Container } from 'react-bootstrap';

import {
  Link,
} from 'react-router-dom';

import './styles.scss';

const NavBar = () => (
  <Navbar className="navbar" expand="md" variant="dark">
    <Container>
      <Link to="/">
        <Navbar.Brand className="brandLogo">
          KK CLASSES
          <div className="tagline">
            आपका लक्ष्य, हमारा लक्ष्य
          </div>
        </Navbar.Brand>
      </Link>
      <Navbar.Toggle aria-controls="responsive-navbar-nav" />
      <Navbar.Collapse id="responsive-navbar-nav">
        <Nav>
          <Link to="/">
              Home
          </Link>
          <Link to="/syllabus">
            Syllabus
          </Link>
          <Link to="/current-affairs">
              Daily Current Affairs
          </Link>
          <Link to="/current-affairs-monthly">
              Monthly Current Affairs
          </Link>
          <Link to="/mock-test">
            Mock Test
          </Link>
        </Nav>
      </Navbar.Collapse>
    </Container>
  </Navbar>
 );

 export default NavBar;
