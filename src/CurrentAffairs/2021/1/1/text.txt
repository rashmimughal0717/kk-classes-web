

  ## 1. हाल ही में हेमंत कुमार पांडे को DRDO के साइंटिस्ट ऑफ़ दी ईयर पुरस्कार से सम्मानित किया गया है
![Hemant Kumar Pandey Archives - News Nukkad](https://i2.wp.com/newsnukkad.com/wp-content/uploads/2020/12/DRDO-Award.jpg?resize=800%2C445&ssl=1)

  ## 2. हाल ही में पोम्पेई इटली में एक प्राचीन स्ट्रीट फ़ूड शॉप की खोज की गयी है
![Pompeii: Ancient 'fast food' counter to open to the public - BBC News](https://c.files.bbci.co.uk/65DC/production/_116267062_tv064955462.jpg)

  ## 3. हाल ही में कर्नाटक की दीप्ती गणपति हेगड़े को इंडिया इंटरनेशनल साइंस फेस्टिवल में प्रथम स्थान मिला है
  ![Karnataka Map | Map of Karnataka State, India | Bengaluru Map](https://files.prokerala.com/maps/karnataka-map.jpg)
  






