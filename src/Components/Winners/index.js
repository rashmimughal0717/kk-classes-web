import React from 'react';

import './styles.scss';

const WINNER_PLACE_ICON = [
  '🥇',
  '🥈',
  '🥉',
]

const Winners = ({
  winners,
}) => (
  <div className="winner-wrapper">
    <ul>
    {
      winners.map(
        (winner, index) => (
          <li>
            <h5><span alt="medal">{WINNER_PLACE_ICON[index]}</span> {winner}</h5>
          </li>
        )
      )
    }
    </ul>
  </div>
)

export default Winners;
