

  ## 1. 'STAR' प्रॉजेक्ट खबरों में है ये शिक्षा मंत्रालय के अधीन आता है 
![What is World Bank's STARS Project?](https://img.jagranjosh.com/imported/images/E/GK/world-bank-stars-project-in-india.jpg)

  ## 2. हाल ही में जारी 'भ्रष्टाचार धारणा सूचांक' में भारत का स्थान 86वा रहा है 
![2020 Corruption Perceptions Index reveals… - Transparency.org](https://images.transparencycdn.org/images/_1200x630_crop_center-center_82_none/2020-CPI-image.jpg?mtime=1611747464)

  ## 3. हाल ही में काजा कलास एस्तोनिया की पहली महिला प्रधानमंत्री बानी है 
![Kaja Kallas to becomes first female Prime Minister of Estonia - TopprsIQ](https://www.topprsiq.com/wp-content/uploads/2021/01/Kaja-Kallas-to-becomes-first-female-Prime-Minister-of-Estonia-720x400.jpg)

  ## 4. हाल ही में विज्ञापन मानक परिषद द्वारा ,चुप न बैठो, डिजिटल अभियान की शुरुआत की है 
![ASCI toughens stance against objectionable ads; urges consumers to  #ChupNaBaitho - Adgully.com](https://www.adgully.com/img/800/202101/chupnabaitho.jpg)



