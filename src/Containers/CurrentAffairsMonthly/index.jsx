import React, { useState, useCallback, useEffect } from 'react';
import { Redirect } from 'react-router';
import { useParams, useHistory } from 'react-router-dom';

import ImportantDates from 'Components/ImportantDates';
import importantDates from 'static/importantDates.json';

import moment from 'moment';

import {
  Container
} from 'react-bootstrap';

import MarkDownText from 'Components/MarkDownText';
import CurrentAffairsNavigator from 'Components/CurrentAffairsNavigator';

import './styles.scss';

function getURL(month, year) {
  return `/current-affairs-monthly/${parseInt(month)}/${parseInt(year)}/`
}

const CurrentAffairs = () => {
  const history = useHistory();
  let { month, year } = useParams();

  const gotoDate = useCallback(
    (day) => (
      moment(`${month}/${year}`, 'M/YYYY').add(day, "months").format('D/M/YYYY').split('/')
    ), [month, year]
  );

  const onChangeDate = useCallback(
    (changeDirection) => {
      const [, month, year] = gotoDate(changeDirection);
      history.push(getURL(month, year));
    },
    [gotoDate, history]
  );

  if (!(month && year)) {
    const currentDate = moment();
    // Jan = 0;
    // by adding 1 making Jan = 1
    month = currentDate.month() + 1;
    year = currentDate.year();
    return (
      <Redirect to={getURL(month, year)} />
    )
  }

  const onDateConfirm = (newDate) => {
    const [year, month] = newDate.split('-');
    history.push(getURL(month, year));
  }

  return (
    <>
      <div className="CANavigationWrapper">
        <CurrentAffairsNavigator
         month={month}
         year={year}
         onDateConfirm={onDateConfirm}
         onChangeDate={onChangeDate}
        />
      </div>
      <Container>
        <MarkDownText
          filePath={`CurrentAffairs/${year}/${month}/text.txt`}
        />
        <ImportantDates
          title="महत्वपूर्ण दिवस"
          importantDateData={importantDates[month]}
        />
      </Container>
    </>
  );
}

export default CurrentAffairs;
