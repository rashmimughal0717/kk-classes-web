

  ## 1. हाल ही में निखिल श्रीवास्तव(भारतीय गणितज्ञ) को माइकल एंड शीला हेल्ड पुरस्कार 2021 से सम्मानित किया गया है
![Indian mathematician Nikhil Srivastava named joint winner of Michael and  Sheila Held Prize | India News,The Indian Express](https://images.indianexpress.com/2021/01/Nikhil-Srivastava-linkedin.jpg)

  ## 2. हाल ही में ITBP ने 10वी राष्ट्रीय आइस हॉकी चैंपियनशिप जीती है
![ITBP wins IHAI National Ice Hockey Championship in Gulmarg](https://englishtribuneimages.blob.core.windows.net/gallary-content/2021/1/2021_1$largeimg_1923767219.jpg)

  ## 3. हाल ही में बहुत बड़े पैमाने पर 'कवच' युद्ध अभ्यास का आयोजन अंडमान और निकोबार में किया जायेगा
![Andaman and Nicobar Islands Map | Nicobar islands, Andaman and nicobar  islands, Andaman islands](https://i.pinimg.com/originals/fd/06/e1/fd06e1d1e16f2f04b0f8c3531ec20055.gif)

  ## 4. विश्व प्रसिद्ध 'Crack K9 QRT' आजकल खबरों में है इसका सम्बन्ध ITBP से है
![ITBP's crack K9 commandos to secure Rajpath on Republic Day](https://i.ytimg.com/vi/_KIANPRoka8/maxresdefault.jpg)
