

    
![100 number with targets and arrows ⬇ Stock Photo, Image by © Pixelery.com  #128731134](https://st3.depositphotos.com/1006899/12873/i/950/depositphotos_128731134-stock-photo-100-number-with-targets-and.jpg)  
   # **रूपए का पुरस्कार** 
   #### साप्ताहिक ऑनलाइन परीक्षा के प्रथम स्थान वाले छात्र को दिया जाता है 


  ---
  
# **हमारे पुरस्कृत छात्र** 
- **रेखा गुप्ता(अरंगी)** (1 बार)




  ---

  ## ✅ हाल ही में 'त्सांग यिंग हंग(हॉन्गकॉंग-चीन)' एवेरेस्ट पर सबसे तेज़ चढ़ाई करने वाली विश्व की प्रथम महिला बन गयी है 
![Hong Kong's Tsang Yin-hung breaks record for fastest ascent of Everest |  The Standard](https://www.thestandard.com.hk/images/instant_news/20210528/20210528130527contentPhoto1.jpg)

  ## ✅ हाल ही में 'सी एन आर राव' को अक्षय ऊर्जा अनुसंधान के लिए 'एनी पुरस्कार 2020' से सम्मानित किया गया है 
![C.N.R. Rao | Indian Scientist | C.N.R. Rao Contributions | YoGems](http://www.yogems.com/wp-content/uploads/2016/02/C.N.R.-Rao_1.jpg)

  ## ✅ हाल ही में 'बशर अल असद' चौथी बार सीरिया के राष्ट्रपति बने है 
![Watch Bashar Al Assad: Master Of Chaos | Prime Video](https://images-na.ssl-images-amazon.com/images/S/sgp-catalog-images/region_US/iglzg-PTZVKQX9DSP-Full-Image_GalleryBackground-en-US-1589222362072._SX1080_.jpg)

  ## ✅ हाल ही में 'अमृत्य सेन' को प्रिंसेस ऑफ़ एस्टोरियस पुरस्कार से सम्मानित किया गया है  
![India no match for China on social indicators: Amartya Sen](https://i.ndtvimg.com/progold/295514_image_main.jpg)

  ## ✅ हाल ही में अरुण वेंकटरमण को अमेरिका और विदेश वाणिज्यिक सेवा के महानिदेशक के रूप में नियुक्त किया गया है 
![Biden selects Eric Garcetti as ambassador to India; nominates Arun  Venkataraman to key position - EasternEye](https://www.easterneye.biz/wp-content/uploads/2021/05/Arun-Venkataraman-1280x720-1-1024x576.jpg)









  ---

![SSC GD Constable Test Series, Paper & Questions](https://www.careerpower.in/images/ssc-gd-exam-mobile.png)
  # **SSC GD Mock परीक्षा**

👉 [https://forms.gle/KCtLnAJ9qu6XUCqz8](https://forms.gle/KCtLnAJ9qu6XUCqz8)
  
 ---

![Top Current Affairs 2019 with answers: September 3 to September 9, 2019 -  Education Today News](https://akm-img-a-in.tosshub.com/indiatoday/images/story/201909/current_affairs_new_0.png?8dmp2f4HrwVzAaVuMKuF9RJ_HkBmoZGT&size=1200:675)
  ### **मई प्रतिदिन करंट अफेयर्स प्रैक्टिस सेट**
  👉 https://forms.gle/XgHzrQHqjYoC9nAe8

  ### **अप्रैल करंट अफेयर्स प्रैक्टिस सेट**
  👉 [https://forms.gle/cz3FcLmnL7KMNj8h7](https://forms.gle/cz3FcLmnL7KMNj8h7)

  ---

![6 Simple Steps to Achieve Any Goal and Create a Life You Love : Plain  Jane's Guide to Happiness](https://plainjanesguidetohappiness.com/wp-content/uploads/2021/01/Archer-2-1024x538.png)
  # **पिछले सप्ताह के टॉपर्स**
  
  **🥇** - रेखा गुप्ता (अरंगी)
  
  **🥈** - सिकंदर प्रसाद (बिंदपुरवा)
  
  **🥉** - राजेश पाल (बसुका)

  ---

