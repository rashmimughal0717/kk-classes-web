






  ## ✅ हाल ही में चीन ने तिब्बत में पहली बार बुलेट ट्रेन की शुरुआत की है 
![China approves building of third railway line in &#39;Tibet&#39; | Tibetan Review](http://www.tibetanreview.net/wp-content/uploads/2014/11/Railway-line-copy.gif)

  ## ✅ हाल ही में रस्किन बांड ने 'इट्स अ वंडरफुल लाइफ' नामक पुस्तक लिखी है  
![It&#39;s a Wonderful Life: Reading Ruskin Bond&#39;s Collection of Vignettes,  Essays and Lockdown Journals - TheSeer](https://theseer.in/wp-content/uploads/2021/05/0001-788248603_20210504_124202_00005910976780423167205.png)






  ### **जून प्रतिदिन करंट अफेयर्स प्रैक्टिस सेट**
  👉 https://forms.gle/9BmF8pEKceeATvf67

  # **SSC GD Mock परीक्षा**
  👉 [https://forms.gle/rVBSR1PuSXdHEeco6](https://forms.gle/rVBSR1PuSXdHEeco6)

  ---


    
![100 Text High Res Stock Images | Shutterstock](https://image.shutterstock.com/image-vector/halftone-round-spot-100-text-260nw-1077968105.jpg)  
   # **रूपए का पुरस्कार** 
   #### साप्ताहिक ऑनलाइन परीक्षा के प्रथम स्थान वाले छात्र को दिया जाता है 

 ---

![5 Real Life Steps To Achieve Your Financial Goals – Internet Lifestyle Hub  – Freedom Blog](https://blog.internetlifestylehub.com/wp-content/uploads/2020/12/financial-goals.jpg)
  # **पिछले सप्ताह के टॉपर्स**

  - 🥇राहुल सिंह (नवली)
  
  - 🥈मनोज कुशवाहा (नवली)
  
  - 🥉रामकृपाल मिश्रा (नवली)

  ---

# **हमारे पुरस्कृत छात्र** 
- **रेखा गुप्ता(अरंगी)** (1 बार)
- **राहुल सिंह (नवली)** (2 बार)
- **सिकंदर प्रसाद (बिंदपुरवा)** (1 बार) 
- **राजेश पाल (बसुका)** (1 बार) 
- **रामकृपाल मिश्रा (नवली)** (1 बार)

  ---
