















  ## ✅ हाल ही में KK बिरला फॉउंडेशन द्वारा कविताओं के संग्रह 'मई तो यहाँ हु' के लिए रामदर्श मिश्रा को प्रतिष्ठित 30वें सरस्वती सम्मान 2021 से सम्मानित किया गया है
![रामदरश मिश्र की 5 सुपरहिट कहानियां by Ramdarash Mishra](https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1535186517l/41454493._SY475_.jpg)

  ## ✅ हाल ही में हरीश मेहता ने 'द मेवरिक इफ़ेक्ट' नामक पुस्तक लिखी है
![Harish Mehta's book shows why NASSCOM is a catalyst in India's start-up  world. It's a treat](https://static.theprint.in/wp-content/uploads/2022/04/DO-NOT-DELETE-THIS-.jpg?compress=true&quality=80&w=376&dpr=2.6)

  ## ✅ हाल ही में दिल्ली सरकार ने सरकारी स्कूल के बच्चों के लिए 'हॉबी हब्स' स्थापित करने की घोषणा की है
![Hub Hobby - Hub Hobby](https://cdn.shoplightspeed.com/shops/628164/themes/3581/v/15706/assets/banner_right.png?20201006150321)




  # **[प्रतिदिन प्रैक्टिस सेट]**
  👉 https://forms.gle/dZP1MpruLEm2UgB3A
