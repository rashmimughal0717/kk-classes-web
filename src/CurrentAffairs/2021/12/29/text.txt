










  ## ✅ हाल ही में विद्युत् मोहन,अनिल प्रकाश जोशी और रिद्धिमा पांडेय को मदर टेरेसा मेमोरियल पुरस्कार 2021 से सम्मानित किया गया है
![Unsung social crusaders awarded Mother Teresa Memorial Awards - Hindustan  Times](https://images.hindustantimes.com/rf/image_size_960x540/HT/p2/2019/11/04/Pictures/_70b7da10-ff02-11e9-96b3-72dd611148b5.jpg)

  ## ✅ हाल ही में भारत सरकार और जर्मन बैंक ने सूरत मेट्रो रेल परियोजना के लिए 442.26 मिलियन यूरो के ऋण को मंजूरी दी है
![PM modi Ahmedabad Surat metro projects bhoomi pujan ceremony | India News –  India TV](https://resize.indiatvnews.com/en/resize/newbucket/715_-/2021/01/gujarat-metro-1610937361.jpg)

  ## ✅ हाल ही में केंद्र सरकार ने Z Plus कैटेगरी में महिला कमांडो को शामिल करने की घोषणा की है
![Women CRPF Commandos For VIPs With Z+ Security | First Women CRPF Commando  Team For VIP Z+ Security - YouTube](https://i.ytimg.com/vi/BRwyNLGx2k4/maxresdefault.jpg)

  ## ✅ हाल ही में ओडिशा राज्य सरकार ने 'मुख्यमंत्री वायु स्वास्थ सेवा एयर एम्बुलेंस' योजना की शुरुआत की है
![Odisha CM launches Mukhyamantri Vayu Swasthya Seva: Here&#39;s all you need to  know](https://sambadenglish.com/wp-content/uploads/2021/12/Mukhyamantri-Vayu-Swasthya-Seva-750x430.jpg)

  ## ✅ हाल ही में केंद्र सरकार ने अप्रैल 2022 से 100 प्रतिशत साक्षरता के लिए 'नव भारत साक्षरता अभियान' को शुरू किया है
![प्रधानमंत्री ग्रामीण डिजिटल साक्षरता अभियान - PMGDISHA](https://hindi.nvshq.org/wp-content/uploads/2020/12/Pradhanmantri-Gramin-Digital-Saksharta-Abhiyan.png)

  ## ✅ हाल ही में भारतीय सेना ने अपने आतंरिक संचार के लिए ASIGMA स्वदेशी मेस्सेंजिंग एप को लांच किया है
![ASIGMA 2021: Indian Army launched in-house messaging app](https://st.adda247.com/https://wpassets.adda247.com/wp-content/uploads/multisite/sites/5/2021/12/27071950/cixzzox2pdwqy9js_1640270703.jpeg)





  # **[प्रतिदिन प्रैक्टिस सेट]**
  👉 https://forms.gle/gLJKrwLG8a7g8wiN9
