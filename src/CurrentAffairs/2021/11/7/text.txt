













  ## ✅ हाल ही में ब्रिटेन देश ने कोरोना संक्रमण के इलाज के लिए Molnupiravir नामक एक वायरल गोली को मंजूरी दी है
![UK approves COVID antiviral pill: What we know about US approval,  eligibility and cost - CNET](https://www.cnet.com/a/img/kjNgCRR8af_9nPJN-gEMv0nmtIQ=/1200x675/2021/10/28/960e239f-d04c-4fd3-ae2f-30eb3bda3c3f/molnupiravir-composite-medication-covid-19-merck-2021-gettyimages-183369961.jpg)

  ## ✅ हाल ही में पंजाब राज्य ने सिंधु नदी डॉलफिन के जनगणना को मंजूरी दी है
![Punjab to begin Census of Indus River Dolphin - Read basic facts about Indus  River Dolphin here for current affairs exam! - GKToday](https://www.gktoday.in/wp-content/uploads/2021/07/dolphins.png)

  ## ✅ हाल ही में आयोजित विश्व बधिर(Deaf) जूडो चैंपियनशिप में भारत देश प्रथम स्थान पर आया है
![Birmingham Announced as Host of 2019 Commonwealth Judo Championships -  British Judo](https://www.britishjudo.org.uk/wp-content/uploads/2018/12/Commonwealth-Judo-scaled.jpg)

  ## ✅ हाल ही में अमित रंजन ने 'जॉन लैंग:वंडरर ऑफ़ हिंदुस्तान' नामक पुस्तक लिखी है
![A book on Rani Laxmibai&#39;s lawyer John Lang authored by Amit Ranjan](https://st.adda247.com/https://wpassets.adda247.com/wp-content/uploads/multisite/sites/5/2021/11/02080909/EU_3tplUwAABTPR-1.jpg)

  ## ✅ हाल ही में Yahoo कंपनी चीन देश से अपना कारोबार बंद करने की घोषणा की है
![Yahoo to rename rump company to Altaba after Verizon sale completes |  Digital | Campaign India](https://cdn.i.haymarketmedia.asia/?n=campaign-india%2Fcontent%2Fyahoo.jpeg&h=570&w=855&q=100&v=20170226&c=1)

  ## ✅ हाल ही में अरुण चावला को FICCI का महानिदेशक नियुक्त किया गया है
![FICCI Full Form - Tutorial And Example](https://www.tutorialandexample.com/wp-content/uploads/2021/06/FICCI-Full-Form.png)

  ## ✅ हाल ही में अमेरिका देश ने दिवाली त्यौहार को अपना राष्ट्रीय अवकाश घोषित किया है
![USA to Declare Diwali as National Holiday ? - YouTube](https://i.ytimg.com/vi/SWJhttQTs-w/maxresdefault.jpg)








  # **[प्रतिदिन प्रैक्टिस सेट]**
  👉 https://forms.gle/1hevTKBybTP2HEJJ8
