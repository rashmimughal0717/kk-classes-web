import React from 'react';
import {
  HashRouter as Router,
  Switch,
  Route,
} from 'react-router-dom';

import './App.scss';

import CurrentAffairs from './Containers/CurrentAffairs';
import CurrentAffairsMonthly from './Containers/CurrentAffairsMonthly';
import Home from './Containers/Home';
import Syllabus from './Containers/Syllabus';
import AboutUs from './Containers/AboutUs';
import NavBar from './Components/NavBar';
import Footer from './Components/Footer';
import MockTest from 'Containers/MockTest';

const App = () => (
  <Router>
    <div className='main-section'>
      <NavBar/>
      <Switch>
        <Route path="/about-us" component={AboutUs} />
        <Route path="/current-affairs-monthly/:month?/:year?/" component={CurrentAffairsMonthly} />
        <Route path="/current-affairs/:date?/:month?/:year?/" component={CurrentAffairs} />
        <Route path="/mock-test/" component={MockTest} />
        <Route path="/syllabus" component={Syllabus} />
        <Route path="/" component={Home} />
      </Switch>
    </div>
    <Footer />
  </Router>
);

export default App;
