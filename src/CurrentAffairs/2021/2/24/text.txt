

  ## 1. हाल ही में भारत ने मॉरीशस देश के साथ मुक्त व्यापार समझौते पर हस्ताक्षर किया है  
![Where is Mauritius? | Where is Mauritius Located in the World Map](https://www.whereig.com/mauritius/where-is-mauritius.jpg)

  ## 2. हाल ही में नागालैंड की विधानसभा में पहली बार राष्ट्रगान बजाया गया है 
![Tourism in Northeast India - Wikipedia](https://upload.wikimedia.org/wikipedia/en/thumb/9/9b/Northeast_India_States.svg/1200px-Northeast_India_States.svg.png)

  ## 3. हाल ही में कर्नाटक राज्य में खेलो इंडिया यूनिवर्सिटी गेम्स 2021 का आयोजन किया जायेगा 
![Karnataka Map | Map of Karnataka State, India | Bengaluru Map](https://files.prokerala.com/maps/karnataka-map.jpg)


