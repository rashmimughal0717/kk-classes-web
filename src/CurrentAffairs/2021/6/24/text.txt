

  ## ✅ हाल ही में दिल्ली सरकार ने 'योग विज्ञान' में पाठ्यक्रम शुरू किया है  
![Yoga Center| दिल्ली ध्यान और योग विज्ञान केंद्र योजना 2021: Apply &amp; Benefits](https://pmkisanyojanalist.in/wp-content/uploads/2021/06/delhi-yoga-center-news-min-1024x551.png)



  ### **जून प्रतिदिन करंट अफेयर्स प्रैक्टिस सेट**
  👉 https://forms.gle/9BmF8pEKceeATvf67

  # **SSC GD Mock परीक्षा**
  👉 [https://forms.gle/9554SdMfcKp4YsUYA](https://forms.gle/9554SdMfcKp4YsUYA)

  ---


    
![100 Text High Res Stock Images | Shutterstock](https://image.shutterstock.com/image-vector/halftone-round-spot-100-text-260nw-1077968105.jpg)  
   # **रूपए का पुरस्कार** 
   #### साप्ताहिक ऑनलाइन परीक्षा के प्रथम स्थान वाले छात्र को दिया जाता है 

 ---

![5 Ways to Motivate Yourself to Work Harder | Inc.com](https://www.incimages.com/uploaded_files/image/1920x1080/candidate-fairness_32620.jpg)
  # **पिछले सप्ताह के टॉपर्स**

  - 🥇रामकृपाल मिश्रा (नवली)
  
  - 🥈राहुल सिंह (नवली)
  
  - 🥉सिकंदर प्रसाद (बिंदपुरवा)

  ---

# **हमारे पुरस्कृत छात्र** 
- **रेखा गुप्ता(अरंगी)** (1 बार)
- **राहुल सिंह (नवली)** (1 बार)
- **सिकंदर प्रसाद (बिंदपुरवा)** (1 बार) 
- **राजेश पाल (बसुका)** (1 बार) 
- **रामकृपाल मिश्रा (नवली)** (1 बार)

  ---
