import React, {
  useEffect,
  useState,
} from 'react';

import {
  Alert,
  Spinner,
} from 'react-bootstrap';

import ReactMarkdown from 'react-markdown';

import './styles.scss';

const MarkDownText = ({
  title,
  filePath,
}) => {
  const [text, setText] = useState();
  const [isLoading, setIsLoading] = useState(true);

  useEffect(
    () => {
      try {
        const textFile = require(`../../${filePath}`);
        fetch(textFile.default).then(
          (result) => (result.text())
        ).then(
          text => {
            setText(text);
            setIsLoading(false);
          }
        );
      } catch (error) {
        console.log('error: current affairs file not found')
        setText(null)
        setIsLoading(false);
      }
    }, [filePath]
  )

  if (!isLoading && !text) {
    return <Alert
      className="no-data"
      variant="warning"
    >
      NO CURRENT AFFAIRS AVAILABLE
    </Alert>
  }

  if (isLoading) {
    return <div className="loader">
        <Spinner animation="border" role="status">
      </Spinner>
      Loading, please wait...
    </div>
  }

  return (
    <div className="current-affairs">
      <div className="notes">
        {
          title &&
          <h1>
            <strong>
              {title}
            </strong>
            <br/>
          </h1>
        }
          <ReactMarkdown children={text} />
      </div>
    </div>
  );
}

export default MarkDownText;
