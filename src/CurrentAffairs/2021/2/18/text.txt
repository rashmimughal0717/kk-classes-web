

  ## 1. हाल ही में उत्तर प्रदेश सरकार ने 'अभ्युदय योजना' की शुरुआत की है 
![Image result for uttar pradesh map](https://i.pinimg.com/originals/4d/28/72/4d287263b42702b20d0d4c808f14cd03.jpg)

  ## 2. हाल ही में थियोडोर भासकरण ने 'सैंक्चुअरी लाइफटाइम अवार्ड 2020' जीता है
![Image result for theodor bhaskaran](https://upload.wikimedia.org/wikipedia/commons/c/cc/Baskaran.JPG)

  ## 3. हाल ही में भारत और म्यांमार के बिच 'कलादान मल्टी मॉडल ट्रांसपोर्ट प्रॉजेक्ट' की शुरुआत की जाएगी
![Image result for kaladan project](https://upload.wikimedia.org/wikipedia/commons/thumb/1/10/Kaladan_Multi-Modal_Transit_Transport_Project.svg/1200px-Kaladan_Multi-Modal_Transit_Transport_Project.svg.png)
